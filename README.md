SCRIPT70: Permission denied with Iframes and Google Translate

We have a page which is using iframes to load different application in. 
By applications I mean different moduls from the same domain.

When we implement Google translate it's fine on the initial page load, but when we change the source of the iFrame SCRIPT70 Permission denied error is thrown.
This will happen when the page is refreshed and will also happen when translation is triggered.
This is causing translation to stop, and display newly loaded document in original language.

The code is tested in Chrome and Safari and it works perfect in those browsers, but in IE9, IE10 and IE11 we get SCRIPT70 Permission denied.




What is the expected behavior? (bug reporting guidelines)
In Chrome and Safari there is no issue thrown and page functions normally.


Please clone this repository: 
1. Open terminal and type npm install
2. In the same terminal window type npm start
3. Open Internet explorer and navigate to http://localhost:3000
4. Change the language in the drop down
5. In the Iframe on the page navigate to iFrame 2 - SCRIPT70 Permission denied error will be thrown


