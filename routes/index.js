var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.sendfile("index.html");
});

router.get("/frame1", function (req, res) {
	res.sendfile("./public/frame1.html");
});

router.get("/frame2", function (req, res) {
	res.sendfile("./public/frame2.html");
});

module.exports = router;
